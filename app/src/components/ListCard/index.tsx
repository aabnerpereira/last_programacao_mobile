import { TouchableOpacityProps } from 'react-native'
import { Dashboard } from '../../pages/Dashboard';

import {
    Container,
    TextTitle,
    TextCard
} from './styles'

interface ListInvoiceProps {
    id: string,
    description: string,
    invoice_value: string,
    issue_date: Date,
    client: string,
}

interface ListCardProps extends TouchableOpacityProps {
    item: ListInvoiceProps;
}

export function ListCard({ item, ...rest }: ListCardProps) {
    
    function pis(invoice_value: number) {
        return (invoice_value * 0.65 / 100).toFixed(2)
    }

    function confis(invoice_value: number) {
        return (invoice_value * 3 / 100).toFixed(2)
    }

    function csll(invoice_value: number) {
        return (invoice_value * 1 / 100).toFixed(2)
    }

    function iss(invoice_value: number) {
        return (invoice_value * 4 / 100).toFixed(2)
    }

    function invoice_valueReceivable(invoice_value: number) {
        return invoice_value - parseFloat(pis(invoice_value)) - parseFloat(confis(invoice_value)) - parseFloat(csll(invoice_value)) - parseFloat(iss(invoice_value))
    }

    function convertDate(issue_date: Date) {
        // const dataArray = data.split('/')
        return new Date(issue_date).toLocaleDateString()
      }

    return (
        <Container
            {...rest}
            key={item.id}>
            <TextTitle>Dados da Nota Fiscal</TextTitle>
            <TextCard>Descrição do Serviço: {item.description}</TextCard>
            <TextCard>Valor da NF: {item.invoice_value}</TextCard>
            <TextCard>Data da NF: {convertDate(item.issue_date)}</TextCard>
            <TextCard>Cliente: {item.client}</TextCard>
            <TextCard>Pis: {pis(Number(item.invoice_value))}</TextCard>
            <TextCard>Confis: {confis(Number(item.invoice_value))}</TextCard>
            <TextCard>Csll: {csll(Number(item.invoice_value))}</TextCard>
            <TextCard>Iss: {iss(Number(item.invoice_value))}</TextCard>
            <TextCard>Salario a Receber: {invoice_valueReceivable(Number(item.invoice_value))}</TextCard>
        </Container>
    )
}


