import { Entity, CreateDateColumn, UpdateDateColumn, PrimaryColumn, Column } from 'typeorm'

import { v4 as uuid } from 'uuid' // identificador universal unico

@Entity('notafiscal')
class NotaFiscal {

  @PrimaryColumn()
  id: string;

  @Column()
  description: string;

  @Column()
  invoice_value: number;

  @Column()
  issue_date: Date;

  @Column()
  client: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  constructor() {
    if (!this.id) {
      this.id = uuid()
    }
  }
}

export { NotaFiscal }
