import { Request, Response } from 'express'
import { NotaFiscalServices } from '../services/NotaFiscalServices'

class NotaFiscalController {

  async create(request: Request, response: Response) {
    let { description, invoice_value, issue_date, client } = request.body
    const payrollServices = new NotaFiscalServices()
    try {
      const payroll = await payrollServices.create({ description, invoice_value, issue_date, client })
      return response.json(payroll)
    } catch (err) {
      return response
        .status(400)
        .json({ message: err.message })
    }
  }

  async index(request: Request, response: Response) {
    const payrollServices = new NotaFiscalServices()

    try {
      const payroll = await payrollServices.index()
      return response.json(payroll)
    } catch (err) {
      return response
        .status(400)
        .json({ message: err.message })
    }
  }

  async delete(request: Request, response: Response) {
    const payrollServices = new NotaFiscalServices()
    const { id } = request.params

    try {
      const payroll = await payrollServices.delete({ id })
      return response.json({ message: 'Nota Fiscal id deleted successfully !!' })
    } catch (err) {
      return response
        .status(400)
        .json({ message: err.message })
    }
  }
}

export { NotaFiscalController }

