import { getCustomRepository } from 'typeorm'
import { PayrollRepository } from '../repositories/PayrollRepository'

interface IPayrollCreate {
  registration: string;
  name: string;
  occupation: string;
  salary: number;
}

interface IPayrollShow {
  id: string
}

class PayrollServices {

  async create({ registration, name, occupation, salary }: IPayrollCreate) {

    const payrollRepository = getCustomRepository(PayrollRepository)
    const payroll = payrollRepository.create({
      registration,
      name,
      occupation,
      salary,
    })

    await payrollRepository.save(payroll)

    return payroll
  }

  async index() {
    const payrollRepository = getCustomRepository(PayrollRepository)

    const payroll = await payrollRepository.find()

    return payroll;
  }

  async delete({ id }: IPayrollShow) {
    const payrollRepository = getCustomRepository(PayrollRepository)

    const payroll = await payrollRepository.findOne({ id })

    if (!payroll) {
      throw new Error('Payroll id not found!!')
    }

    return await payrollRepository.delete({ id })
  }

}

export { PayrollServices }