import { getCustomRepository } from 'typeorm'
import { NotaFiscalRepository } from '../repositories/NotaFiscalRepository'

interface INotaFiscalCreate {
  description: string;
  invoice_value: number;
  issue_date: Date;
  client: string;
}

interface INotaFiscalShow {
  id: string
}

class NotaFiscalServices {

  async create({ description, invoice_value, issue_date, client }: INotaFiscalCreate) {

    const payrollRepository = getCustomRepository(NotaFiscalRepository)
    const payroll = payrollRepository.create({
      description,
      invoice_value,
      issue_date,
      client,
    })

    await payrollRepository.save(payroll)

    return payroll
  }

  async index() {
    const payrollRepository = getCustomRepository(NotaFiscalRepository)

    const payroll = await payrollRepository.find()

    return payroll;
  }

  async delete({ id }: INotaFiscalShow) {
    const payrollRepository = getCustomRepository(NotaFiscalRepository)

    const payroll = await payrollRepository.findOne({ id })

    if (!payroll) {
      throw new Error('Nota Fiscal id not found!!')
    }

    return await payrollRepository.delete({ id })
  }

}

export { NotaFiscalServices }