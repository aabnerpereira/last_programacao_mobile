import { Router } from 'express'

import { PayrollController } from './controllers/PayrollController'
import { NotaFiscalController } from './controllers/NotaFiscalController'

const routes = Router();

const payrollController = new PayrollController()
const notaFiscalController = new NotaFiscalController()

routes.post('/payroll', payrollController.create)
routes.get('/payroll', payrollController.index)
routes.delete('/payroll/:id', payrollController.delete)

routes.post('/invoice', notaFiscalController.create)
routes.get('/invoice', notaFiscalController.index)
routes.delete('/invoice/:id', notaFiscalController.delete)

export { routes }

