import { Repository, EntityRepository } from 'typeorm'
import { NotaFiscal } from '../entities/NotaFiscal'

@EntityRepository(NotaFiscal)
class NotaFiscalRepository extends Repository<NotaFiscal> {

}

export { NotaFiscalRepository }

